﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit;
using NUnit.Framework;
using PayrollPreparation;

namespace PayrollPreparation.UnitTest
{
    [TestFixture]
    public class BLLTests
    {
        [TestCase(2018, 06, 15, 8267279)]
        [TestCase(2006, 06, 06, 8218118)]
        [TestCase(2222, 12, 31, 9104415)]
        public void DateToInt(int year, int month, int day, int result)
        {
            DateTime date = new DateTime(year, month, day);
            Assert.That(result == new BLL().DateToInt(date));
        }

        [TestCase(8267279, 2018, 06, 15)]
        [TestCase(8218118, 2006, 06, 06)]
        [TestCase(9104415, 2222, 12, 31)]
        public void IntToDate(int value, int year, int month, int day)
        {
            DateTime date = new DateTime(year, month, day);
            Assert.That(date == new BLL().IntToDate(value));
        }

        [TestCase(1000, 2018, 2030, 1300.0)]
        [TestCase(2000, 2006, 2015, 2540.00)]
        [TestCase(10000, 2015, 2015, 10000.0)]
        public void CalculateSalaryWorker(int salary, int yearFrom, int yearTo, decimal result)
        {
            Employee employee = new Worker() { Salary = salary, EmploymentDate = new BLL().DateToInt(new DateTime(yearFrom, 1, 1)) };
            Assert.That(employee.CalculateSalary(new DateTime(yearTo, 1, 1)) == result);
        }

        [TestCase(1100, 2018, 2030, 1300.0)]
        public void CalculateSalaryManager(int salary, int yearFrom, int yearTo, decimal result)
        {
            Employee employee = new Manager() { Salary = salary, EmploymentDate = new BLL().DateToInt(new DateTime(yearFrom, 1, 1)) };
            var r = employee.CalculateSalary(new DateTime(yearTo, 1, 1));
            Assert.That(true);
            Assert.That(employee.CalculateSalary(new DateTime(yearTo, 1, 1)) == result);
        }

        [TestCase(1000, 2018, 2030, 1300.0)]
        [TestCase(2000, 2006, 2015, 2540.00)]
        [TestCase(10000, 2015, 2015, 10000.0)]
        public void CalculateSalarySalesman(int salary, int yearFrom, int yearTo, decimal result)
        {
            Employee employee = new Salesman() { Salary = salary, EmploymentDate = new BLL().DateToInt(new DateTime(yearFrom, 1, 1)) };
            var r = employee.CalculateSalary(new DateTime(yearTo, 1, 1));
            Assert.That(employee.CalculateSalary(new DateTime(yearTo, 1, 1)) == result);
        }
    }
}
