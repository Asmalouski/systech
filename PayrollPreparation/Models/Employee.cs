﻿using System;

namespace PayrollPreparation
{
    public abstract class Employee
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int EmploymentDate { get; set; }
        public decimal Salary { get; set; }
        public int IdGroup { get; set; }
        public int? IdChief { get; set; }

        public abstract decimal CalculateSalary(DateTime period);

        public override string ToString()
        {
            string Chief = IdChief.HasValue? IdChief.Value.ToString() : null;
            return $"{Id};{Login};{Password};{FirstName};{LastName};{EmploymentDate};{Salary};{IdGroup};{Chief}";
        }
    }
}
