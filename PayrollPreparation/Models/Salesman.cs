﻿using System;
using System.Collections.Generic;

namespace PayrollPreparation
{
    public class Salesman : Employee, ISubordinates<Employee>
    {
        public override decimal CalculateSalary(DateTime period)
        {
            int years = period.Year - new BLL().IntToDate(this.EmploymentDate).Year;

            int timeBonus = years > 35 ? 35 : years;

            decimal bonusForSubordinates = 0;
            foreach (Employee item in GetSubordinates())
            {
                bonusForSubordinates += (decimal)(item.Salary) * 3 / 1000;
            }

            return Math.Round(Salary + (Salary * timeBonus / 100) + bonusForSubordinates, 2);
        }

        //this is a bad implementation. I will do normal implementation if I do not sleeping
        public List<Employee> GetSubordinates() =>
             new EmployeeRepository().GetAllItems().FindAll(p => p.IdChief == Id);
    }
}
