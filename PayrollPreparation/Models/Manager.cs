﻿using System;
using System.Collections.Generic;

namespace PayrollPreparation
{
    public class Manager : Employee, ISubordinates<Employee>
    {
        public override decimal CalculateSalary(DateTime period)
        {
            int years = period.Year - new BLL().IntToDate(this.EmploymentDate).Year;

            decimal timeBonus = years * 5 > 40 ? (decimal)(40) /100 : (decimal)(years) * 5/100;

            decimal bonusForSubordinates = 0;
            foreach (Employee item in GetSubordinates())
            {
                bonusForSubordinates += (decimal)(item.Salary * 5 / 1000);
            }

            return Math.Round(Salary + Salary * timeBonus + bonusForSubordinates, 2);
        }

        //this is a bad implementation. I will do normal implementation if I do not sleeping
        public List<Employee> GetSubordinates() =>
             new EmployeeRepository().GetAllItems().FindAll(p => p.IdChief == Id);
    }
}
