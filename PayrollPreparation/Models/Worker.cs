﻿using System;

namespace PayrollPreparation
{
    public class Worker : Employee
    {
        public override decimal CalculateSalary(DateTime period)
        {
            int years = period.Year - new BLL().IntToDate(this.EmploymentDate).Year;

            decimal timeBonus = years * 3 > 30 ? (decimal)30 / 100 : (decimal)years * 3 / 100;

            return (Salary + Salary * timeBonus);
        }
    }
}
