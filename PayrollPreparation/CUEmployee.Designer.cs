﻿namespace PayrollPreparation
{
    partial class CUEmployee
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Chiefl = new System.Windows.Forms.Label();
            this.Groupl = new System.Windows.Forms.Label();
            this.Salaryl = new System.Windows.Forms.Label();
            this.EmploymentDate = new System.Windows.Forms.Label();
            this.LastNamel = new System.Windows.Forms.Label();
            this.FirstNamel = new System.Windows.Forms.Label();
            this.Passwordl = new System.Windows.Forms.Label();
            this.Logl = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.FirstName = new System.Windows.Forms.TextBox();
            this.LastName = new System.Windows.Forms.TextBox();
            this.Log = new System.Windows.Forms.TextBox();
            this.Pass = new System.Windows.Forms.TextBox();
            this.Salary = new System.Windows.Forms.TextBox();
            this.Date = new System.Windows.Forms.DateTimePicker();
            this.Group = new System.Windows.Forms.ComboBox();
            this.Chief = new System.Windows.Forms.ComboBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.IsChief = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // Chiefl
            // 
            this.Chiefl.AutoSize = true;
            this.Chiefl.Font = new System.Drawing.Font("Times New Roman", 16.75F);
            this.Chiefl.Location = new System.Drawing.Point(18, 359);
            this.Chiefl.Name = "Chiefl";
            this.Chiefl.Size = new System.Drawing.Size(69, 26);
            this.Chiefl.TabIndex = 28;
            this.Chiefl.Text = "Chief:";
            // 
            // Groupl
            // 
            this.Groupl.AutoSize = true;
            this.Groupl.Font = new System.Drawing.Font("Times New Roman", 16.75F);
            this.Groupl.Location = new System.Drawing.Point(18, 319);
            this.Groupl.Name = "Groupl";
            this.Groupl.Size = new System.Drawing.Size(78, 26);
            this.Groupl.TabIndex = 27;
            this.Groupl.Text = "Group:";
            // 
            // Salaryl
            // 
            this.Salaryl.AutoSize = true;
            this.Salaryl.Font = new System.Drawing.Font("Times New Roman", 16.75F);
            this.Salaryl.Location = new System.Drawing.Point(18, 279);
            this.Salaryl.Name = "Salaryl";
            this.Salaryl.Size = new System.Drawing.Size(76, 26);
            this.Salaryl.TabIndex = 26;
            this.Salaryl.Text = "Salary:";
            // 
            // EmploymentDate
            // 
            this.EmploymentDate.AutoSize = true;
            this.EmploymentDate.Font = new System.Drawing.Font("Times New Roman", 16.75F);
            this.EmploymentDate.Location = new System.Drawing.Point(18, 239);
            this.EmploymentDate.Name = "EmploymentDate";
            this.EmploymentDate.Size = new System.Drawing.Size(180, 26);
            this.EmploymentDate.TabIndex = 25;
            this.EmploymentDate.Text = "Employment date:";
            // 
            // LastNamel
            // 
            this.LastNamel.AutoSize = true;
            this.LastNamel.Font = new System.Drawing.Font("Times New Roman", 16.75F);
            this.LastNamel.Location = new System.Drawing.Point(18, 119);
            this.LastNamel.Name = "LastNamel";
            this.LastNamel.Size = new System.Drawing.Size(112, 26);
            this.LastNamel.TabIndex = 24;
            this.LastNamel.Text = "Last name:";
            // 
            // FirstNamel
            // 
            this.FirstNamel.AutoSize = true;
            this.FirstNamel.Font = new System.Drawing.Font("Times New Roman", 16.75F);
            this.FirstNamel.Location = new System.Drawing.Point(18, 79);
            this.FirstNamel.Name = "FirstNamel";
            this.FirstNamel.Size = new System.Drawing.Size(116, 26);
            this.FirstNamel.TabIndex = 23;
            this.FirstNamel.Text = "First name:";
            // 
            // Passwordl
            // 
            this.Passwordl.AutoSize = true;
            this.Passwordl.Font = new System.Drawing.Font("Times New Roman", 16.75F);
            this.Passwordl.Location = new System.Drawing.Point(18, 199);
            this.Passwordl.Name = "Passwordl";
            this.Passwordl.Size = new System.Drawing.Size(108, 26);
            this.Passwordl.TabIndex = 22;
            this.Passwordl.Text = "Password:";
            // 
            // Logl
            // 
            this.Logl.AutoSize = true;
            this.Logl.Font = new System.Drawing.Font("Times New Roman", 16.75F);
            this.Logl.Location = new System.Drawing.Point(18, 159);
            this.Logl.Name = "Logl";
            this.Logl.Size = new System.Drawing.Size(72, 26);
            this.Logl.TabIndex = 21;
            this.Logl.Text = "Login:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 28.75F);
            this.label1.Location = new System.Drawing.Point(70, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(277, 43);
            this.label1.TabIndex = 20;
            this.label1.Text = "Create employee";
            // 
            // FirstName
            // 
            this.FirstName.Location = new System.Drawing.Point(210, 78);
            this.FirstName.Name = "FirstName";
            this.FirstName.Size = new System.Drawing.Size(200, 32);
            this.FirstName.TabIndex = 37;
            // 
            // LastName
            // 
            this.LastName.Location = new System.Drawing.Point(210, 118);
            this.LastName.Name = "LastName";
            this.LastName.Size = new System.Drawing.Size(200, 32);
            this.LastName.TabIndex = 38;
            // 
            // Log
            // 
            this.Log.Location = new System.Drawing.Point(210, 158);
            this.Log.Name = "Log";
            this.Log.Size = new System.Drawing.Size(200, 32);
            this.Log.TabIndex = 39;
            // 
            // Pass
            // 
            this.Pass.Location = new System.Drawing.Point(210, 198);
            this.Pass.Name = "Pass";
            this.Pass.Size = new System.Drawing.Size(200, 32);
            this.Pass.TabIndex = 40;
            // 
            // Salary
            // 
            this.Salary.Location = new System.Drawing.Point(210, 278);
            this.Salary.Name = "Salary";
            this.Salary.Size = new System.Drawing.Size(200, 32);
            this.Salary.TabIndex = 41;
            this.Salary.Leave += new System.EventHandler(this.Salary_Leave);
            // 
            // Date
            // 
            this.Date.Location = new System.Drawing.Point(210, 239);
            this.Date.Name = "Date";
            this.Date.Size = new System.Drawing.Size(200, 32);
            this.Date.TabIndex = 42;
            // 
            // Group
            // 
            this.Group.FormattingEnabled = true;
            this.Group.Location = new System.Drawing.Point(210, 318);
            this.Group.Name = "Group";
            this.Group.Size = new System.Drawing.Size(200, 31);
            this.Group.TabIndex = 43;
            // 
            // Chief
            // 
            this.Chief.FormattingEnabled = true;
            this.Chief.Location = new System.Drawing.Point(210, 358);
            this.Chief.Name = "Chief";
            this.Chief.Size = new System.Drawing.Size(200, 31);
            this.Chief.TabIndex = 44;
            // 
            // button2
            // 
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Times New Roman", 16.75F);
            this.button2.Location = new System.Drawing.Point(293, 420);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(117, 41);
            this.button2.TabIndex = 45;
            this.button2.Text = "Create";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.IndianRed;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Times New Roman", 16.75F);
            this.button1.Location = new System.Drawing.Point(170, 420);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(117, 41);
            this.button1.TabIndex = 46;
            this.button1.Text = "Cancel";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // IsChief
            // 
            this.IsChief.AutoSize = true;
            this.IsChief.Checked = true;
            this.IsChief.CheckState = System.Windows.Forms.CheckState.Checked;
            this.IsChief.Location = new System.Drawing.Point(155, 362);
            this.IsChief.Name = "IsChief";
            this.IsChief.Size = new System.Drawing.Size(43, 27);
            this.IsChief.TabIndex = 47;
            this.IsChief.Text = "Is";
            this.IsChief.UseVisualStyleBackColor = true;
            this.IsChief.CheckedChanged += new System.EventHandler(this.IsChief_CheckedChanged);
            // 
            // CUEmployee
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(428, 487);
            this.Controls.Add(this.IsChief);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.Chief);
            this.Controls.Add(this.Group);
            this.Controls.Add(this.Date);
            this.Controls.Add(this.Salary);
            this.Controls.Add(this.Pass);
            this.Controls.Add(this.Log);
            this.Controls.Add(this.LastName);
            this.Controls.Add(this.FirstName);
            this.Controls.Add(this.Chiefl);
            this.Controls.Add(this.Groupl);
            this.Controls.Add(this.Salaryl);
            this.Controls.Add(this.EmploymentDate);
            this.Controls.Add(this.LastNamel);
            this.Controls.Add(this.FirstNamel);
            this.Controls.Add(this.Passwordl);
            this.Controls.Add(this.Logl);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(12, 9, 12, 9);
            this.Name = "CUEmployee";
            this.Text = "CreateEmployee";
            this.Load += new System.EventHandler(this.CreateEmployee_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Chiefl;
        private System.Windows.Forms.Label Groupl;
        private System.Windows.Forms.Label Salaryl;
        private System.Windows.Forms.Label EmploymentDate;
        private System.Windows.Forms.Label LastNamel;
        private System.Windows.Forms.Label FirstNamel;
        private System.Windows.Forms.Label Passwordl;
        private System.Windows.Forms.Label Logl;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox FirstName;
        private System.Windows.Forms.TextBox LastName;
        private System.Windows.Forms.TextBox Log;
        private System.Windows.Forms.TextBox Pass;
        private System.Windows.Forms.TextBox Salary;
        private System.Windows.Forms.DateTimePicker Date;
        private System.Windows.Forms.ComboBox Group;
        private System.Windows.Forms.ComboBox Chief;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox IsChief;
    }
}