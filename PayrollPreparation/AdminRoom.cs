﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Windows.Forms;

namespace PayrollPreparation
{
    public partial class AdminRoom : FormPattern
    {
        Form LastForm;
        public AdminRoom(Form form)
        {
            InitializeComponent();
            this.LastForm = form;
        }

        private void AdminRoom_Load(object sender, EventArgs e)
        {
            string cmd = $"select Employees.Id, Employees.FirstName, Employees.LastName, Employees.Salary, GroupsOfEmployees.Name AS Status from Employees, GroupsOfEmployees where GroupsOfEmployees.Id = Employees.IdGroup";

            //очищаем DGV
            dataGridView1.Columns.Clear();
            //подключаемся и выполняем команду
            SQLiteDataAdapter dataAdapter = new SQLiteDataAdapter(cmd, new Database().Connection);
            //перенос данных в переменную
            System.Data.DataTable ds = new System.Data.DataTable();
            //заполняем таблицу данными из базы данных
            dataAdapter.Fill(ds);
            dataGridView1.DataSource = ds;
            dataGridView1.Columns[0].Visible = false;
            foreach (DataGridViewColumn item in dataGridView1.Columns)
            {
                item.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                int id = Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value);
                Employee user = new EmployeeRepository().Read(id);
                new PersonalRoomOfEmployee(this, user).ShowDialog();
            }
            catch (System.NullReferenceException)
            {
                MessageBox.Show("Item is not selected");
            }
        }

        private void AdminRoom_FormClosing(object sender, FormClosingEventArgs e)
        {
            LastForm.Visible = true;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            decimal totalSalary = 0, totalCleanSalary = 0;
            List<Employee> employees = new BLL().GetEmployees();

            foreach (Employee item in employees)
            {
                totalCleanSalary += item.Salary;
                totalSalary += item.CalculateSalary(dateTimePicker1.Value);
            }

            MessageBox.Show($"Salary for the period: {dateTimePicker1.Value.ToShortDateString()}\n\rTotal salary without bonuses: {Math.Round(totalCleanSalary, 2)}$\n\rBonuses: {Math.Round(totalSalary - totalCleanSalary, 2)}$\n\rTotal: {Math.Round(totalSalary, 2)}$");
        }

        private void AdminRoom_FormClosed(object sender, FormClosedEventArgs e) => LastForm.Visible = true;

        private void button2_Click(object sender, EventArgs e)
        {
            new CUEmployee().ShowDialog();
            AdminRoom_Load(sender, e);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            new CUEmployee(new EmployeeRepository().Read(Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value))).ShowDialog();
            AdminRoom_Load(sender, e);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            DialogResult res = new DialogResult();
            res = MessageBox.Show("Do you really want to dismiss the employee?", "Dismiss", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (res == DialogResult.Yes)
            {
                new BLL().DeleteEmployee(Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value));
                MessageBox.Show("Successful operation");
            }
            AdminRoom_Load(sender, e);
        }
    }
}
