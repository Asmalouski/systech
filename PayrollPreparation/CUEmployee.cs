﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace PayrollPreparation
{
    public partial class CUEmployee : FormPattern
    {
        Employee UpdateEmployee = null;

        public CUEmployee() => InitializeComponent();

        //If update
        public CUEmployee(Employee employee)
        {
            InitializeComponent();

            button2.Text = "Update";
            label1.Text = "Update employee";

            //Filling fields
            FirstName.Text = employee.FirstName;
            LastName.Text = employee.LastName;
            Log.Text = employee.Login;
            Pass.Text = employee.Password;
            Salary.Text = employee.Salary.ToString();
            Date.Value = new BLL().IntToDate(employee.EmploymentDate);

            UpdateEmployee = employee;
        }
        //Filling comboboxes
        private void CreateEmployee_Load(object sender, EventArgs e)
        {
            Group.Items.Clear();
            List<string> groups = new List<string>();
            List<string> chiefs = new List<string>();

            foreach (var item in new GroupOfEmployeesRepository().GetAllItems())
                groups.Add(item.Name);

            Chief.Items.Clear();
            foreach (var item in new EmployeeRepository().GetAllItems().FindAll(p => p.IdGroup > 2))
                chiefs.Add(item.LastName);

            Group.DataSource = groups;
            Chief.DataSource = chiefs;

            Group.DropDownStyle = Chief.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void IsChief_CheckedChanged(object sender, EventArgs e) => Chief.Enabled = !Chief.Enabled;

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult res = new DialogResult();
            res = MessageBox.Show("Do you really want to leave?", "Exit", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (res == DialogResult.Yes)
                Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(FirstName.Text) | string.IsNullOrEmpty(LastName.Text) | string.IsNullOrEmpty(Log.Text) | string.IsNullOrEmpty(Pass.Text) | System.Text.RegularExpressions.Regex.IsMatch(Salary.Text, @"^\d+[.,]{0,1}\d{1,2}$"))
                    throw new FormatException("The fields are invalid");
                else
                {//Values are correctly
                    if (UpdateEmployee == null)
                    {
                        DialogResult res = new DialogResult();
                        res = MessageBox.Show("Do you really want to create a new employee?", "Create a new employee", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (res == DialogResult.Yes)
                        {
                            int? idChief = null;
                            if (IsChief.Checked)
                                idChief = new BLL().GetEmployees().Find(p => p.LastName == Chief.Text).Id;  ///TODO: Rewrite

                            int idGroup = new BLL().GetGroupOfEmployees().Find(p => p.Name == Group.Text).Id;
                            new BLL().CreateEmployee(Log.Text, Pass.Text, FirstName.Text, LastName.Text, Convert.ToDecimal(Salary.Text), Date.Value, idChief, idGroup);

                            MessageBox.Show("Successful operation");
                            Close();
                        }
                    }
                    else
                    {
                        DialogResult res = new DialogResult();
                        res = MessageBox.Show("Do you really want to update the employee?", "Update the employee", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (res == DialogResult.Yes)
                        {
                            int? idChief = null;
                            if (IsChief.Checked)
                                idChief = new BLL().GetEmployees().Find(p => p.LastName == Chief.Text).Id;
                            int idGroup = new BLL().GetGroupOfEmployees().Find(p => p.Name == Group.Text).Id;

                            new BLL().UpdateEmployee(UpdateEmployee.Id, Log.Text, Pass.Text, FirstName.Text, LastName.Text, Convert.ToDecimal(Salary.Text), Date.Value, idChief, idGroup);

                            MessageBox.Show("Successful operation");
                            Close();
                        }
                    }
                }
            }
            catch (System.FormatException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Salary_Leave(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(Salary.Text, @"^\d+[.,]{0,1}\d{1,2}$"))
                Salary.ForeColor = Color.Green;
            else
                Salary.ForeColor = Color.Red;
        }
    }
}
