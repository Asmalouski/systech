﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayrollPreparation
{
    public class BLL
    {
        //0000 0000 |0000 0000   0000|0000|0000 0000 - int

        //          |            2018|  06|       15
        //          |0111 1110   0010|0110|0000 1111

        //          |            4018|  12|       31
        //          |1111 1011   0010|1100|0001 1111
        //0111 1110   001001100000 1111
        public int DateToInt(DateTime date)
        {
            int year = date.Year, month = date.Month, day = date.Day;
            int value = 0;
            value += year;
            value = value << 4;
            value += month;
            value = value << 8;
            value += day;
            return value;
        }
        public DateTime IntToDate(int date)
        {
            int year = Math.Abs(date >> 12);
            int month = (date >> 8) % 16;
            int day = date % 256;

            return new DateTime(year, month, day);
        }

        public decimal SalariesOfAllEmployees(DateTime period)
        {
            decimal amount = 0;
            foreach (Employee item in new EmployeeRepository().GetAllItems())
            {
                amount += item.CalculateSalary(period);
            }
            return amount;
        }

        public Employee Authorization(string login, string password) => new Database().Authorization(login, password);///TODO: Validation

        #region CRUD
        public void CreateEmployee(string login, string password, string firstName, string lastName, decimal salary, DateTime employmentDate, int? idChief, int idGroup) =>
            new EmployeeRepository().Create(new Worker() { FirstName = firstName, LastName = lastName, EmploymentDate = DateToInt(employmentDate), IdChief = idChief, IdGroup = idGroup, Login = login, Password = password, Salary = salary });

        public Employee UpdateEmployee(int id, string login, string password, string firestName, string lastName, decimal salary, DateTime employmentDate, int? idChief, int idGroup) =>
            new EmployeeRepository().Update(new Worker() { Id = id, Login = login, Password = password, FirstName = firestName, LastName = lastName, EmploymentDate = DateToInt(employmentDate), IdChief = idChief, IdGroup = idGroup, Salary = salary });

        public Employee GetEmployeeById(int id) => new EmployeeRepository().Read(id);
        public Employee GetEmployeeByName(string name) => new EmployeeRepository().Read(name);

        public void DeleteEmployee(int id)
        {
            EmployeeRepository repository = new EmployeeRepository();
            foreach (var item in repository.GetAllItems().FindAll(p => p.IdChief == id))
            {
                item.IdChief = null;
                repository.Update(item);
            }
            new Database().Delete("Employees", id);
        }



        public void CreateGroup(string name) => new GroupOfEmployeesRepository().Create(new GroupOfEmployees() { Name = name });

        public GroupOfEmployees UpdateGroup(int id, string name) => new
GroupOfEmployeesRepository().Update(new GroupOfEmployees() { Id = id, Name = name });

        public GroupOfEmployees GetGroupOfEmployeesById(int id) => new GroupOfEmployeesRepository().Read(id);
        public GroupOfEmployees GetGroupOfEmployeesByName(string name) => new GroupOfEmployeesRepository().Read(name);

        public void DeleteGroupOfEmployees(int id) => new GroupOfEmployeesRepository().Delete(GetGroupOfEmployeesById(id));
        #endregion


        public List<Employee> GetEmployees() => new EmployeeRepository().GetAllItems();
        public List<GroupOfEmployees> GetGroupOfEmployees() => new GroupOfEmployeesRepository().GetAllItems();
    }
}
