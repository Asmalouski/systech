﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data.SQLite;

namespace PayrollPreparation
{
    public class Database
    {
        public SQLiteConnection Connection = null;
        string DatabaseName { get; set; } = "EmployeeDatabase.db";
        string connectionString => $"Data Source = {DatabaseName}";

        public Database()
        {
            SQLiteFactory factory = (SQLiteFactory)DbProviderFactories.GetFactory("System.Data.SQLite");

            Connection = (SQLiteConnection)factory.CreateConnection();
            Connection.ConnectionString = connectionString;
            Connection.Open();
        }

        public void CreateDatabase(string databaseName = "EmployeeDatabase.db")
        {
            DatabaseName = "EmployeeDatabase.db";
            Connection.Dispose();
            SQLiteConnection.CreateFile(DatabaseName);
            Connection.Dispose();
        }

        //read and run all the prepared scripts from the folder
        public void FillingDatabase()
        {
            using (SQLiteCommand command = new SQLiteCommand(Connection))
            {
                string[] dirs = System.IO.Directory.GetFiles("..//..//Database");
                foreach (string dir in dirs)
                {
                    command.CommandText = System.IO.File.ReadAllText(dir);
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                }
            }
            Connection.Dispose();
        }

        public void Delete(string table, int id) => SqlOperation($"DELETE FROM {table} WHERE Id = {id};");


        public List<dynamic> Search(string table, int id)
        {
            List<dynamic> values = new List<dynamic>();
            string strSQL = $"SELECT {table}.* FROM {table} WHERE Id = {id};";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = strSQL;
                    SqlDataReader dataReader = command.ExecuteReader();

                    do
                    {
                        foreach (var item in dataReader)
                        {
                            values.Add(item);
                        }
                    }
                    while (dataReader.Read());
                }
            }
            return values;
        }

        public List<dynamic> GetItems(string table)
        {
            List<dynamic> values = new List<dynamic>();
            string strSQL = $"SELECT {table}.* FROM {table};";

            using (SQLiteCommand command = new SQLiteCommand(new Database().Connection))
            {
                command.CommandText = strSQL;
                SQLiteDataReader dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    values.Add(dataReader);
                }
            }
            return values;
        }

        public Employee Authorization(string login, string password)
        {
            string strSQL = $"SELECT Employees.* FROM Employees WHERE Login='{login}' and Password='{password}';";

            Employee item = null;
            using (SQLiteCommand command = new SQLiteCommand(new Database().Connection))
            {
                command.CommandText = strSQL;
                SQLiteDataReader reader = command.ExecuteReader();

                if (reader.Read())
                {
                    switch (reader.GetInt32(7))
                    {
                        case 1:
                            item = new Worker();
                            break;
                        case 2:
                            item = new Manager();
                            break;
                        case 3:
                            item = new Salesman();
                            break;
                        default:
                            goto case 1;
                    }
                    item.Id = reader.GetInt32(0);
                    item.Login = reader.GetString(1);
                    item.Password = reader.GetString(2);
                    item.FirstName = reader.GetString(3);
                    item.LastName = reader.GetString(4);
                    item.EmploymentDate = reader.GetInt32(5);
                    item.Salary = reader.GetDecimal(6);
                    item.IdGroup = reader.GetInt32(7);

                    if (!reader.IsDBNull(8))
                        item.IdChief = reader.GetInt32(8);
                }
            }
            return item ?? null;
        }


        public void SqlOperation(string sql)
        {
            using (SQLiteCommand command = new SQLiteCommand(new Database().Connection))
            {
                command.CommandText = sql;
                command.CommandType = CommandType.Text;
                command.ExecuteNonQuery();
            }
        }
    }
}
