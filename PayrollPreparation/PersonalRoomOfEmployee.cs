﻿using System;
using System.Windows.Forms;

namespace PayrollPreparation
{
    public partial class PersonalRoomOfEmployee : FormPattern
    {
        Form LastForm;
        Employee Employee;
        public PersonalRoomOfEmployee(Form form, Employee employee)
        {
            InitializeComponent();
            this.LastForm = form;
            Employee = employee;
            button2.Visible = employee.IdGroup != 1;
        }

        private void PersonalRoomOfEmployee_Load(object sender, EventArgs e)
        {
            FirstName.Text = $"{Employee.FirstName}";
            LastName.Text = $"{Employee.LastName}";
            Login.Text = $"{Employee.Login}";
            Password.Text = $"{Employee.Password}";
            Salary.Text = $"{Employee.Salary}";
            Date.Text = $"{new BLL().IntToDate(Employee.EmploymentDate).ToShortDateString()}";
            Group.Text = $"{new GroupOfEmployeesRepository().Read(Employee.IdGroup).Name}";

            if (Employee.IdChief.HasValue)
            {
                Employee chief = new EmployeeRepository().Read(Employee.IdChief.Value);
                Chief.Text = $"{chief.LastName} {chief.FirstName}";
            }
            else
                Chief.Text = "none";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            decimal totalSalary = Employee.CalculateSalary(dateTimePicker1.Value);
            MessageBox.Show($"Salary for the period: {dateTimePicker1.Value.ToShortDateString()}\n\rSalary: {Employee.Salary}$\n\rBonuses: {totalSalary - Employee.Salary}$\n\rTotal: {totalSalary}$");
        }

        private void button2_Click(object sender, EventArgs e) => new Subordinates(Employee).ShowDialog();

        private void PersonalRoomOfEmployee_FormClosed(object sender, FormClosingEventArgs e) => this.LastForm.Visible = true;
    }
}
