﻿using System;
using System.Windows.Forms;

namespace PayrollPreparation
{
    public partial class Authorization : FormPattern
    {
        public Authorization() => InitializeComponent();

        private void pictureBox2_Click(object sender, EventArgs e) => Passsword.PasswordChar = Passsword.PasswordChar == '\0' ? '*' : '\0';

        private void Submit_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(Login.Text) | string.IsNullOrEmpty(Passsword.Text)) ///TODO: Validation
                    throw new FormatException("Fields are not valid");
                else
                {
                    Employee user = new BLL().Authorization(Login.Text, Passsword.Text);

                    if (user == null)
                        throw new FormatException("The user was not found");

                    this.Visible = false;
                    //IdGroup == 4. 4 = admin
                    if (user.IdGroup == 4)
                        new AdminRoom(this).ShowDialog();
                    else
                    {
                        user = new EmployeeRepository().DefinitionOfEmployee(user);
                        new PersonalRoomOfEmployee(this, user).ShowDialog();
                    }
                }
            }
            catch (FormatException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult res = new DialogResult();
                res = MessageBox.Show("Do you want to reset database?", "Reset database", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (res == DialogResult.Yes)
                {
                    System.Data.SQLite.SQLiteConnection.CreateFile("EmployeeDatabase.db");
                    new Database().FillingDatabase();
                    MessageBox.Show("Completed");
                }
            }
            catch (System.IO.IOException)
            {
                MessageBox.Show("Please close connection");
            }
        }
    }
}
