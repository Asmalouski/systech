﻿using System;
using System.Data.SQLite;
using System.Windows.Forms;

namespace PayrollPreparation
{
    public partial class Subordinates : FormPattern
    {
        Employee Employee;
        public Subordinates(Employee employee)
        {
            InitializeComponent();
            Employee = employee;
        }
        private void Subordinates_Load(object sender, EventArgs e)
        {
            string cmd = $"select Employees.Id, Employees.FirstName, Employees.LastName, Employees.Salary, GroupsOfEmployees.Name AS Status from Employees, GroupsOfEmployees where Employees.IdChief = {Employee.Id} and Employees.IdGroup = GroupsOfEmployees.Id";

            //очищаем DGV
            dataGridView1.Columns.Clear();
            //подключаемся и выполняем команду
            SQLiteDataAdapter dataAdapter = new SQLiteDataAdapter(cmd, new Database().Connection);
            //перенос данных в переменную
            System.Data.DataTable ds = new System.Data.DataTable();
            //заполняем таблицу данными из базы данных
            dataAdapter.Fill(ds);
            dataGridView1.DataSource = ds;
            dataGridView1.Columns[0].Visible = false;
            foreach (DataGridViewColumn item in dataGridView1.Columns)
            {
                item.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value);
            Employee employee = new BLL().GetEmployeeById(id);
            MessageBox.Show(employee.CalculateSalary(dateTimePicker1.Value).ToString());
        }
    }
}
