﻿using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;

namespace PayrollPreparation
{
    class EmployeeRepository : IRepository<Employee>
    {

        public string SqlTableName { get; } = "Employees";

        public void Create(Employee item)
        {
            string salary = item.Salary.ToString().Replace(',', '.');
            using (SQLiteCommand command = new SQLiteCommand(new Database().Connection))
            {
                command.CommandText = $"INSERT INTO Employees (Login,  Password,  FirstName,  LastName,  EmploymentDate,  Salary,  IdGroup,  IdChief) VALUES ('{item.Login}', '{item.Password}', '{item.FirstName}', '{item.LastName}', {item.EmploymentDate}, {salary}, {item.IdGroup}, {item.IdChief})";
                command.CommandType = CommandType.Text;
                command.ExecuteNonQuery();
            }
        }

        public Employee Read(int id) => Search($"SELECT {SqlTableName}.* FROM {SqlTableName} WHERE Id = {id};");
        public Employee Read(string name) => Search($"SELECT {SqlTableName}.* FROM {SqlTableName} WHERE LastName = '{name}';");

        public Employee Search(string query)
        {
            Employee item = new Worker();
            using (SQLiteCommand command = new SQLiteCommand(new Database().Connection))
            {
                command.CommandText = query;
                SQLiteDataReader reader = command.ExecuteReader();

                if (reader.Read())
                {
                    switch (item.IdGroup)
                    {
                        case 1:
                            item = fillingItem<Worker>(reader);
                            break;
                        case 2:
                            item = fillingItem<Manager>(reader);
                            break;
                        case 3:
                            item = fillingItem<Salesman>(reader);
                            break;
                        default:
                            goto case 1;
                    }
                }
            }
            return item.Id != 0 ? item : null;
        }


        private T fillingItem<T>(SQLiteDataReader reader) where T : Employee, new()
        {
            T item = new T
            {
                Id = reader.GetInt32(0),
                Login = reader.GetString(1),
                Password = reader.GetString(2),
                FirstName = reader.GetString(3),
                LastName = reader.GetString(4),
                EmploymentDate = reader.GetInt32(5),
                Salary = reader.GetDecimal(6),
                IdGroup = reader.GetInt32(7)
            };
            if (!reader.IsDBNull(8))
                item.IdChief = reader.GetInt32(8);

            return item;
        }

        public Employee Update(Employee item)
        {
            string salary = item.Salary.ToString().Replace(',', '.');
            using (SQLiteCommand command = new SQLiteCommand(new Database().Connection))
            {
                command.CommandText = $"UPDATE {SqlTableName} SET Login = '{item.Login}',  Password='{item.Password}',  FirstName='{item.FirstName}',  LastName='{item.LastName}',  EmploymentDate={item.EmploymentDate},  Salary={salary},  IdGroup={item.IdGroup},  IdChief={item.IdChief} Where Id={item.Id}";
                command.CommandType = CommandType.Text;
                command.ExecuteNonQuery();
            }
            return item;
        }

        public void Delete(Employee employee)
        {
            foreach (var item in GetAllItems().FindAll(p => p.IdChief == employee.Id))
            {
                item.IdChief = null;
                Update(item);
            }
            new Database().Delete(SqlTableName, employee.Id);
        }

        public List<Employee> GetAllItems()
        {
            List<Employee> items = new List<Employee>();

            string strSQL = $"SELECT {SqlTableName}.* FROM {SqlTableName};";

            Employee item = new Worker();
            using (SQLiteCommand command = new SQLiteCommand(new Database().Connection))
            {
                command.CommandText = strSQL;
                SQLiteDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    switch (item.IdGroup)
                    {
                        case 1:
                            item = fillingItem<Worker>(reader);
                            break;
                        case 2:
                            item = fillingItem<Manager>(reader);
                            break;
                        case 3:
                            item = fillingItem<Salesman>(reader);
                            break;
                        default:
                            goto case 1;
                    }
                    items.Add(item);
                }
            }

            return items;
        }

        public Employee DefinitionOfEmployee(Employee item)
        {
            object employee = item;
            switch (item.IdGroup)
            {
                case 1:
                    return (Worker)employee;
                case 2:
                    return (Manager)employee;
                case 3:
                    return (Salesman)employee;
                default:
                    goto case 1;
            }
        }
    }
}
