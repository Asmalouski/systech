﻿using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;

namespace PayrollPreparation
{
    public class GroupOfEmployeesRepository
    {
        public string SqlTableName { get; } = "GroupsOfEmployees";


        public void Create(GroupOfEmployees item)
        {
            SQLiteParameter[] sqlParameters = {
            new SQLiteParameter("@Name", SqlDbType.VarChar) { Value = item.Name } };

            using (SQLiteCommand command = new SQLiteCommand(new Database().Connection))
            {
                command.CommandText = $"INSERT INTO {SqlTableName} (Name) VALUES(@Name)";
                command.CommandType = CommandType.Text;
                command.Parameters.AddRange(sqlParameters);
                command.ExecuteNonQuery();
            }
        }


        public GroupOfEmployees Read(int id) => Search($"SELECT {SqlTableName}.* FROM {SqlTableName} WHERE Id = {id};");
        public GroupOfEmployees Read(string name) => Search($"SELECT {SqlTableName}.* FROM {SqlTableName} WHERE Name = '{name}';");

        GroupOfEmployees Search(string query)
        {
            GroupOfEmployees item = new GroupOfEmployees();
            using (SQLiteCommand command = new SQLiteCommand(new Database().Connection))
            {
                command.CommandText = query;
                SQLiteDataReader reader = command.ExecuteReader();

                if (reader.Read())
                {
                    item = new GroupOfEmployees
                    {
                        Id = reader.GetInt32(0),
                        Name = reader.GetString(1)
                    };
                }
            }
            return item.Id != 0 ? item : null;
        }

        public GroupOfEmployees Update(GroupOfEmployees item)
        {
            using (SQLiteCommand command = new SQLiteCommand(new Database().Connection))
            {
                command.CommandText = $"UPDATE {SqlTableName} SET Name = '{item.Name}' Where Id={item.Id}";
                command.CommandType = CommandType.Text;
                command.ExecuteNonQuery();
            }
            return item;
        }

        public void Delete(GroupOfEmployees item) => new Database().Delete(SqlTableName, item.Id);


        public List<GroupOfEmployees> GetAllItems()
        {
            List<GroupOfEmployees> items = new List<GroupOfEmployees>();


            string strSQL = $"SELECT {SqlTableName}.* FROM {SqlTableName};";

            GroupOfEmployees item = new GroupOfEmployees();
            using (SQLiteCommand command = new SQLiteCommand(new Database().Connection))
            {
                command.CommandText = strSQL;
                SQLiteDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    item = new GroupOfEmployees
                    {
                        Id = reader.GetInt32(0),
                        Name = reader.GetString(1)
                    };
                    items.Add(item);
                }
            }
            return items;
        }
    }
}
