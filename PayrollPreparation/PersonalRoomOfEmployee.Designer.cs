﻿namespace PayrollPreparation
{
    partial class PersonalRoomOfEmployee
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.Logl = new System.Windows.Forms.Label();
            this.Passwordl = new System.Windows.Forms.Label();
            this.LastNamel = new System.Windows.Forms.Label();
            this.FirstNamel = new System.Windows.Forms.Label();
            this.Salaryl = new System.Windows.Forms.Label();
            this.EmploymentDate = new System.Windows.Forms.Label();
            this.Chiefl = new System.Windows.Forms.Label();
            this.Groupl = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.button1 = new System.Windows.Forms.Button();
            this.Login = new System.Windows.Forms.Label();
            this.LastName = new System.Windows.Forms.Label();
            this.FirstName = new System.Windows.Forms.Label();
            this.Chief = new System.Windows.Forms.Label();
            this.Group = new System.Windows.Forms.Label();
            this.Salary = new System.Windows.Forms.Label();
            this.Date = new System.Windows.Forms.Label();
            this.Password = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 28.75F);
            this.label1.Location = new System.Drawing.Point(130, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(256, 43);
            this.label1.TabIndex = 0;
            this.label1.Text = "Personal Room";
            // 
            // Logl
            // 
            this.Logl.AutoSize = true;
            this.Logl.Font = new System.Drawing.Font("Times New Roman", 16.75F);
            this.Logl.Location = new System.Drawing.Point(30, 160);
            this.Logl.Name = "Logl";
            this.Logl.Size = new System.Drawing.Size(72, 26);
            this.Logl.TabIndex = 1;
            this.Logl.Text = "Login:";
            // 
            // Passwordl
            // 
            this.Passwordl.AutoSize = true;
            this.Passwordl.Font = new System.Drawing.Font("Times New Roman", 16.75F);
            this.Passwordl.Location = new System.Drawing.Point(30, 200);
            this.Passwordl.Name = "Passwordl";
            this.Passwordl.Size = new System.Drawing.Size(108, 26);
            this.Passwordl.TabIndex = 2;
            this.Passwordl.Text = "Password:";
            // 
            // LastNamel
            // 
            this.LastNamel.AutoSize = true;
            this.LastNamel.Font = new System.Drawing.Font("Times New Roman", 16.75F);
            this.LastNamel.Location = new System.Drawing.Point(30, 120);
            this.LastNamel.Name = "LastNamel";
            this.LastNamel.Size = new System.Drawing.Size(112, 26);
            this.LastNamel.TabIndex = 4;
            this.LastNamel.Text = "Last name:";
            // 
            // FirstNamel
            // 
            this.FirstNamel.AutoSize = true;
            this.FirstNamel.Font = new System.Drawing.Font("Times New Roman", 16.75F);
            this.FirstNamel.Location = new System.Drawing.Point(30, 80);
            this.FirstNamel.Name = "FirstNamel";
            this.FirstNamel.Size = new System.Drawing.Size(116, 26);
            this.FirstNamel.TabIndex = 3;
            this.FirstNamel.Text = "First name:";
            // 
            // Salaryl
            // 
            this.Salaryl.AutoSize = true;
            this.Salaryl.Font = new System.Drawing.Font("Times New Roman", 16.75F);
            this.Salaryl.Location = new System.Drawing.Point(30, 280);
            this.Salaryl.Name = "Salaryl";
            this.Salaryl.Size = new System.Drawing.Size(76, 26);
            this.Salaryl.TabIndex = 6;
            this.Salaryl.Text = "Salary:";
            // 
            // EmploymentDate
            // 
            this.EmploymentDate.AutoSize = true;
            this.EmploymentDate.Font = new System.Drawing.Font("Times New Roman", 16.75F);
            this.EmploymentDate.Location = new System.Drawing.Point(30, 240);
            this.EmploymentDate.Name = "EmploymentDate";
            this.EmploymentDate.Size = new System.Drawing.Size(180, 26);
            this.EmploymentDate.TabIndex = 5;
            this.EmploymentDate.Text = "Employment date:";
            // 
            // Chiefl
            // 
            this.Chiefl.AutoSize = true;
            this.Chiefl.Font = new System.Drawing.Font("Times New Roman", 16.75F);
            this.Chiefl.Location = new System.Drawing.Point(30, 360);
            this.Chiefl.Name = "Chiefl";
            this.Chiefl.Size = new System.Drawing.Size(69, 26);
            this.Chiefl.TabIndex = 8;
            this.Chiefl.Text = "Chief:";
            // 
            // Groupl
            // 
            this.Groupl.AutoSize = true;
            this.Groupl.Font = new System.Drawing.Font("Times New Roman", 16.75F);
            this.Groupl.Location = new System.Drawing.Point(30, 320);
            this.Groupl.Name = "Groupl";
            this.Groupl.Size = new System.Drawing.Size(78, 26);
            this.Groupl.TabIndex = 7;
            this.Groupl.Text = "Group:";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Font = new System.Drawing.Font("Times New Roman", 20.75F);
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(259, 415);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 39);
            this.dateTimePicker1.TabIndex = 10;
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Times New Roman", 16.75F);
            this.button1.Location = new System.Drawing.Point(32, 413);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(205, 44);
            this.button1.TabIndex = 11;
            this.button1.Text = "Calculate salary for";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Login
            // 
            this.Login.AutoSize = true;
            this.Login.Font = new System.Drawing.Font("Times New Roman", 16.75F);
            this.Login.Location = new System.Drawing.Point(240, 160);
            this.Login.Name = "Login";
            this.Login.Size = new System.Drawing.Size(0, 26);
            this.Login.TabIndex = 12;
            // 
            // LastName
            // 
            this.LastName.AutoSize = true;
            this.LastName.Font = new System.Drawing.Font("Times New Roman", 16.75F);
            this.LastName.Location = new System.Drawing.Point(240, 120);
            this.LastName.Name = "LastName";
            this.LastName.Size = new System.Drawing.Size(0, 26);
            this.LastName.TabIndex = 14;
            // 
            // FirstName
            // 
            this.FirstName.AutoSize = true;
            this.FirstName.Font = new System.Drawing.Font("Times New Roman", 16.75F);
            this.FirstName.Location = new System.Drawing.Point(240, 80);
            this.FirstName.Name = "FirstName";
            this.FirstName.Size = new System.Drawing.Size(0, 26);
            this.FirstName.TabIndex = 13;
            // 
            // Chief
            // 
            this.Chief.AutoSize = true;
            this.Chief.Font = new System.Drawing.Font("Times New Roman", 16.75F);
            this.Chief.Location = new System.Drawing.Point(240, 360);
            this.Chief.Name = "Chief";
            this.Chief.Size = new System.Drawing.Size(0, 26);
            this.Chief.TabIndex = 19;
            // 
            // Group
            // 
            this.Group.AutoSize = true;
            this.Group.Font = new System.Drawing.Font("Times New Roman", 16.75F);
            this.Group.Location = new System.Drawing.Point(240, 320);
            this.Group.Name = "Group";
            this.Group.Size = new System.Drawing.Size(0, 26);
            this.Group.TabIndex = 18;
            // 
            // Salary
            // 
            this.Salary.AutoSize = true;
            this.Salary.Font = new System.Drawing.Font("Times New Roman", 16.75F);
            this.Salary.Location = new System.Drawing.Point(240, 280);
            this.Salary.Name = "Salary";
            this.Salary.Size = new System.Drawing.Size(0, 26);
            this.Salary.TabIndex = 17;
            // 
            // Date
            // 
            this.Date.AutoSize = true;
            this.Date.Font = new System.Drawing.Font("Times New Roman", 16.75F);
            this.Date.Location = new System.Drawing.Point(240, 240);
            this.Date.Name = "Date";
            this.Date.Size = new System.Drawing.Size(0, 26);
            this.Date.TabIndex = 16;
            // 
            // Password
            // 
            this.Password.AutoSize = true;
            this.Password.Font = new System.Drawing.Font("Times New Roman", 16.75F);
            this.Password.Location = new System.Drawing.Point(240, 200);
            this.Password.Name = "Password";
            this.Password.Size = new System.Drawing.Size(0, 26);
            this.Password.TabIndex = 15;
            // 
            // button2
            // 
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Times New Roman", 16.75F);
            this.button2.Location = new System.Drawing.Point(32, 477);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(205, 44);
            this.button2.TabIndex = 40;
            this.button2.Text = "Subordinates";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // PersonalRoomOfEmployee
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 561);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.Chief);
            this.Controls.Add(this.Group);
            this.Controls.Add(this.Salary);
            this.Controls.Add(this.Date);
            this.Controls.Add(this.Password);
            this.Controls.Add(this.LastName);
            this.Controls.Add(this.FirstName);
            this.Controls.Add(this.Login);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.Chiefl);
            this.Controls.Add(this.Groupl);
            this.Controls.Add(this.Salaryl);
            this.Controls.Add(this.EmploymentDate);
            this.Controls.Add(this.LastNamel);
            this.Controls.Add(this.FirstNamel);
            this.Controls.Add(this.Passwordl);
            this.Controls.Add(this.Logl);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(12, 9, 12, 9);
            this.MinimumSize = new System.Drawing.Size(500, 400);
            this.Name = "PersonalRoomOfEmployee";
            this.Text = "PersonalRoom";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PersonalRoomOfEmployee_FormClosed);
            this.Load += new System.EventHandler(this.PersonalRoomOfEmployee_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label Logl;
        private System.Windows.Forms.Label Passwordl;
        private System.Windows.Forms.Label LastNamel;
        private System.Windows.Forms.Label FirstNamel;
        private System.Windows.Forms.Label Salaryl;
        private System.Windows.Forms.Label EmploymentDate;
        private System.Windows.Forms.Label Chiefl;
        private System.Windows.Forms.Label Groupl;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label Login;
        private System.Windows.Forms.Label LastName;
        private System.Windows.Forms.Label FirstName;
        private System.Windows.Forms.Label Chief;
        private System.Windows.Forms.Label Group;
        private System.Windows.Forms.Label Salary;
        private System.Windows.Forms.Label Date;
        private System.Windows.Forms.Label Password;
        private System.Windows.Forms.Button button2;
    }
}