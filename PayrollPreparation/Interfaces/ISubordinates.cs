﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayrollPreparation
{
    interface ISubordinates<T>
    {
        List<T> GetSubordinates();
    }
}
