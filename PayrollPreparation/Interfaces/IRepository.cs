﻿namespace PayrollPreparation
{
    interface IRepository<T>
    {
        void Create(T item);
        T Update(T item);
        T Read(int id);
        void Delete(T item);

        System.Collections.Generic.List<T> GetAllItems();
    }
}
