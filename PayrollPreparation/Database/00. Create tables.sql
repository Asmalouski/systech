CREATE TABLE Employees (
                    Id integer PRIMARY KEY AUTOINCREMENT NOT NULL,
                    Login char(30) NOT NULL,
                    Password char(30) NOT NULL,
                    FirstName char(30) NOT NULL,
                    LastName char(30) NOT NULL,
                    EmploymentDate int NOT NULL,
                    Salary real NOT NULL,
                    IdGroup int NOT NULL,
                    IdChief int NULL,
                    FOREIGN KEY(IdGroup) REFERENCES GroupsOfEmployees(Id),
                    FOREIGN KEY(IdChief) REFERENCES Employees(Id));

CREATE TABLE GroupsOfEmployees (
                    Id integer PRIMARY KEY AUTOINCREMENT,
                    Name text NOT NULL);