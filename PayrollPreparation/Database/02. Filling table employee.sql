INSERT INTO Employees (Login, Password, FirstName,LastName,EmploymentDate,Salary,IdGroup)
VALUES 
( 'admin', 'admin', 'Sergei', 'Maksimenko', 8193551, 5000.0, 4 ),
( 'salesman', 'salesman', 'Olga', 'Dobromenchatenko', 8218118, 2500.0, 3);

INSERT INTO Employees (Login, Password,FirstName,LastName,EmploymentDate,Salary,IdGroup, IdChief)
VALUES 
( 'manager', 'manager', 'Jo', 'Carlin', 8218118, 2000, 2, 2 ),
( 'employee', 'employee', 'Key', 'Rott', 8267279, 1050.5, 1, 2),
( 'e2', 'e2', 'Andry', 'Poll', 8267279, 1500, 1, 3 ),
( 'e3', 'e3', 'Ivan', 'Doll', 8267279, 1200, 1, 2 );